package gofunc

import (
	"fmt"
	"testing"
)

func TestAnyOf(t *testing.T) {
	var l []int =  []int{1, 2, 3, 4, 5, 6 }
	// l :=  [...]int{1, 2, 3, 4, 5, 6 }

	is_even := func(i int) bool { if i % 2 == 0 { return true } else { return false } }
	b := AnyOf(l, is_even)
	if b != true {
		t.Fatalf("AnyOf(%v, is_even) should be true", l)
	}

	b = AnyOf([]int{1,3,5,7,9}, is_even)
	if b == true {
		t.Fatalf("AnyOf({1,3,5,7,9}, is_even) should be false")
	}

	fmt.Printf("%v\n", b)
}
