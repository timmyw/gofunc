package gofunc

func AnyOf[T any](ts []T, f func(T) bool) bool {
	for _, t := range ts {
		if f(t) == true {
			return true
		}
	}
	return false
}
