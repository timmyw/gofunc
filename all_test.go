package gofunc

import (
	"testing"
)

func TestAll(t *testing.T) {
	ts := []int{2,4,6}
	is_even := func(i int) bool { if i % 2 == 0 { return true } else { return false } }
	b := All(ts, is_even)
	if b != true {
		t.Fatalf("All({2,4,6}, is_even) should return true")
	}

	ts = []int{2,4,5}
	b = All(ts, is_even)
	if b != false {
		t.Fatalf("All({2,4,5}, is_even) should return false")
	}

}
