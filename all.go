package gofunc

func All[T any](ts []T, f func(T) bool) bool {
	for _, t := range ts {
		if f(t) == false {
			return false
		}
	}
	return true
}

func None[T any](ts []T, f func(T) bool) bool {
	return !(All(ts, f))
}
