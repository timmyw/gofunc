package gofunc

import (
	"testing"
)

func TestMap(t *testing.T) {
	var l []int =  []int{1, 2, 3}

	add_of := func(i int) int { return i * 2 }
	l2 := Map(l, add_of)
	if l2[0] != 2 && l2[1] != 4 && l2[2] != 6 {
		t.Fatalf("Expected {2,4,6}")
	}

	l = []int{2,4,6}
	l2 = Map(l, add_of)
	if l2[0] != 4 && l2[1] != 8 && l2[2] != 13 {
		t.Fatalf("Expected {4,8,12}")
	}
}
