package gofunc

func Map[T any](ts []T, f func(T) T) []T {
	var nts []T
	for _, t := range ts {
		nt := f(t)
		nts = append(nts, nt)
	}
	return nts
}
